﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControlador : MonoBehaviour
{
    private PlayerModelo m;
    private void Start()
    {
        m=GetComponent<PlayerModelo>();
    }
    // Start is called before the first frame update
  public  void Mover(){
                Cursor.lockState = CursorLockMode.Locked;
        m.h = m.horizontalSpeed * Input.GetAxis("Mouse X");
        m.v = m.verticalSpeed * Input.GetAxis("Mouse Y");
        transform.Rotate(0, m.h, 0);
        m.FPSCamera.transform.Rotate(-m.v, 0, 0);
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

       m.rb.velocity = (transform.forward * m.speed * vertical) + (transform.right * m.speed * horizontal);


        if (Input.GetKeyDown(KeyCode.Mouse0))
        {

            if (m.balas > 0)
            {
                GameObject obj = Instantiate(m.bulletPrefab);
                obj.transform.position = m.shootOrigin.position;
                obj.GetComponent<Bullet>().direction = m.shootOrigin.transform.forward;
                m.balas--;
                m.municionesText.GetComponent<Text>().text = "Balas: " + m.balas;

            }
        }

        
    }
}
