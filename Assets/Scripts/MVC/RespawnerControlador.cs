﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnerControlador : MonoBehaviour
{
    private RespawnerModelo mod;
    private void Start()
    {
        mod=GetComponent<RespawnerModelo>();
    }
    // Start is called before the first frame update
    public void EnemigoRespawn() {
        int value = Random.Range(1, 6);
        for (int i = 0; i < value; i++) {
            GameObject obj = Instantiate(mod.enemigo);
            obj.transform.position=mod.spawnPoints[Random.Range(0, mod.spawnPoints.Length)].transform.position;
            obj.GetComponent<seguirplayer>().origin= mod.spawnPoints[Random.Range(0, mod.spawnPoints.Length)].transform.position;
        }
}}


