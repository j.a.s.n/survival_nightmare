﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class HudVista : MonoBehaviour
{
    private HudModelo hudM;
    
    // Start is called before the first frame update
    void Start()
    {
        hudM = GetComponent<HudModelo>();
        


    }
    // Update is called once per frame
    void Update()
    {
            Contador();
    }

    public void Contador()
    {
        hudM.gasolinaText.GetComponent<Text>().text = hudM.gasolina + " / 3";
        hudM.vidaText.GetComponent<Text>().text = "Vida: " + hudM.live;
        hudM.municionesText.GetComponent<Text>().text = "Balas: " + hudM.balas;
    }


}



