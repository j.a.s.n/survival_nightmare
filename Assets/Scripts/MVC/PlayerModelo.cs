﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModelo : MonoBehaviour
{
    public Camera FPSCamera;
    public float horizontalSpeed;
    public float verticalSpeed;
    public float speed;
    public Rigidbody rb;
    public Sprite spr1;
    public Sprite spr2;
    public SpriteRenderer spriterenderer;
    public int live;
    public float v;
    public float h;
    public int balas;
    public int gasolina;

    public GameObject municionesText;
    public GameObject gasolinaText;
    public GameObject vidaText;


    public Transform shootOrigin;
    public GameObject bulletPrefab;
}
