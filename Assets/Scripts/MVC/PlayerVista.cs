﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PlayerVista : MonoBehaviour
{
    private PlayerModelo playerm;
    private PlayerControlador playerc;
    // Start is called before the first frame update
 void Start()
    {
        playerm=GetComponent<PlayerModelo>();
        playerc=GetComponent<PlayerControlador>();
        playerm.gasolinaText.GetComponent<Text>().text = playerm.gasolina + " / 3";
       playerm. vidaText.GetComponent<Text>().text = "Vida: " + playerm.live;
       playerm. municionesText.GetComponent<Text>().text = "Balas: " + playerm.balas;
        playerm.rb = GetComponent<Rigidbody>();
       playerm. spriterenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
  playerc.Mover();

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {


            playerm.live -= 1;

           playerm. vidaText.GetComponent<Text>().text = "Vida: " + playerm.live;

        }




        if (playerm.live <= 0)
        {

            SceneManager.LoadScene(3);
        }


    }
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Municion"))
        {
            playerm.balas += collision.gameObject.GetComponent<Municiones>().balas;
            playerm.municionesText.GetComponent<Text>().text = "Balas: " + playerm.balas;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Gasolina"))
        {
           playerm. gasolina += collision.gameObject.GetComponent<Gasolina>().gasolina;
           playerm. gasolinaText.GetComponent<Text>().text = playerm.gasolina + "/3";
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Medicina") && playerm.live < 5.0f)
        {
            playerm.live += collision.gameObject.GetComponent<Vida>().live;
            playerm.vidaText.GetComponent<Text>().text = "Vida: " + playerm.live;
            Destroy(collision.gameObject);
        }
        else if(collision.gameObject.CompareTag("Medicina") && playerm.live == 5.0f)
        {
           playerm. live += 1;
            playerm.vidaText.GetComponent<Text>().text = "Vida: " + playerm.live;
            Destroy(collision.gameObject);
        }
}
}

