﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnerVista : MonoBehaviour
{
    private RespawnerModelo resm;
    private RespawnerControlador controlador;
    // Start is called before the first frame update
     public void Start() {
         resm=GetComponent<RespawnerModelo>();
         controlador=GetComponent<RespawnerControlador>();
        resm.spawnPoints = GameObject.FindGameObjectsWithTag("EnemySpawn");
        controlador.InvokeRepeating("EnemigoRespawn", 0f,8f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
