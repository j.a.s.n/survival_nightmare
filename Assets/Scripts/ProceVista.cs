﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceVista : Procemodelo
{
    
    // Start is called before the first frame update
    void Start()
    {
        
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                int index = map[i, j];
                if (index != -1)
                    Instantiate(tiles[1],
                        new Vector3(j * 30f, 0, i * -30f),
                        Quaternion.identity);
            }
        }
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                int index = map[i, j];
                if (index != -1)
                    Instantiate(tiles[1],
                        new Vector3(j * 30f, 30, i * -30f),
                        Quaternion.identity);
            }
        }
    }
}
