﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Procemodelo : MonoBehaviour
{
    public GameObject[] tiles;
    public int[,] map = new int[8, 8]
    {
         {-1,-1,-1,-1,-1,-1,-1,-1 },
        {-1,-1,-1,-1,1,2,2,3 },
        { -1,-1,-1,1,9,7,9,0},
        {-1,-1,-1,4,2,3,1,6 },
        { -1,1,2,6,7,8,8,9},
        {1,5,6,0,-1,-1,-1,-1 },
        {4,5,6,0,-1,-1,-1,-1},
        {7,9,7,9,-1,-1,-1,-1 }

    };
}
