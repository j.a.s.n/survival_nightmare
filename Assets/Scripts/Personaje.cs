﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Personaje : MonoBehaviour
{
    public Camera FPSCamera;
    public float horizontalSpeed;
    public float verticalSpeed;
    public float speed;
    public Rigidbody rb;
    public Sprite spr1;
    public Sprite spr2;
    public SpriteRenderer spriterenderer;
    public int live;
    float v;
    float h;
    public int balas;
    public int gasolina;

    public GameObject municionesText;
    public GameObject gasolinaText;
    public GameObject vidaText;


    public Transform shootOrigin;
    public GameObject bulletPrefab;

    public float time = 1;

    // Start is called before the first frame update
    void Start()
    {
        gasolinaText.GetComponent<Text>().text = gasolina + " / 3";
        vidaText.GetComponent<Text>().text = "Vida: " + live;
        municionesText.GetComponent<Text>().text = "Balas: " + balas;
        rb = GetComponent<Rigidbody>();
        spriterenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

        time -= Time.deltaTime;

        Cursor.lockState = CursorLockMode.Locked;
        h = horizontalSpeed * Input.GetAxis("Mouse X");
        v = verticalSpeed * Input.GetAxis("Mouse Y");
        transform.Rotate(0, h, 0);
        FPSCamera.transform.Rotate(-v, 0, 0);
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        rb.velocity = (transform.forward * speed * vertical) + (transform.right * speed * horizontal);


        if (Input.GetKeyDown(KeyCode.Mouse0) && time <= 0)
        {

            if (balas > 0)
            {
                GameObject obj = Instantiate(bulletPrefab);
                obj.transform.position = shootOrigin.position;
                obj.GetComponent<Bullet>().direction = shootOrigin.transform.forward;
                balas--;
                municionesText.GetComponent<Text>().text = "Balas: " + balas;

                time = 1;

            }
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {


            live -= 1;

            vidaText.GetComponent<Text>().text = "Vida: " + live;

        }




        if (live <= 0)
        {

            SceneManager.LoadScene(3);
        }


    }
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Municion"))
        {
            balas += collision.gameObject.GetComponent<Municiones>().balas;
            municionesText.GetComponent<Text>().text = "Balas: " + balas;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Gasolina"))
        {
            gasolina += collision.gameObject.GetComponent<Gasolina>().gasolina;
            gasolinaText.GetComponent<Text>().text = gasolina + "/3";
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Medicina") && live < 5.0f)
        {
            live += collision.gameObject.GetComponent<Vida>().live;
            vidaText.GetComponent<Text>().text = "Vida: " + live;
            Destroy(collision.gameObject);
        }
        else if(collision.gameObject.CompareTag("Medicina") && live == 5.0f)
        {
            live += 1;
            vidaText.GetComponent<Text>().text = "Vida: " + live;
            Destroy(collision.gameObject);
        }


    }
}
