﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class seguirplayer : MonoBehaviour
{
    public Transform jugador;
    UnityEngine.AI.NavMeshAgent enemigo;
    private bool dentro = false;

    public int live;
    // Start is called before the first frame update
    void Start()
    {
        enemigo = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            dentro = true;
        }
        if (other.tag=="Bullet")
        {
            live -= 1;
            Destroy(other.gameObject,0);

            if (live <= 0)
            {
                Destroy(gameObject);
            }
        }
        else
        {
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            dentro = false;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (!dentro)
        {
            enemigo.destination = jugador.position;
        }
        if (dentro)
        {
            enemigo.destination = this.transform.position;
        }

    }
    
}
