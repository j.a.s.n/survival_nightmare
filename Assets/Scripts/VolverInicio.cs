﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class VolverInicio : MonoBehaviour
{
    private void Start()
    {

        Cursor.lockState = CursorLockMode.None;
    }

    // Start is called before the first frame update
    public void boton()
    {
        SceneManager.LoadScene(0);
    }

    // Update is called once per frame
    
}
